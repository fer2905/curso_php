<?php
session_start();
$varsesion = $_SESSION['Alumno'];
if($varsesion == null || $varsesion = ''){
  header('http://127.0.0.1/Curso%20PHP/tarea2_LFPH/login.php');
die();

}
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="formulario.css">
  <title>Formulario</title>
</head>

<body>
  <nav>
    <div class="navegar">
      <a href="#" class="btn btn-secondary btn-lg active btn-outline-info" role="button" aria-pressed="true">Inicio</a>
      <a href="#" class="btn btn-secondary btn-lg active btn-outline-info" role="button" aria-pressed="true">Registro</a>

    </div>
    <div class="cerrar">
      <a href="#" class=" btn btn-secondary  btn btn-info" role="button">Cerrar sesión</a>
    </div>

  </nav>
  <form action="info.php" method="POST">
    <div class="input-group mb-3">

      <input type="number" class="form-control" placeholder="Numero de cuenta" name="numCta">
     >
    </div>
    <div class="input-group mb-3">

      <input type="text" class="form-control" placeholder="Nombre" name="nombre">
    </div>
    <div class="input-group mb-3">

      <input type="text" class="form-control" placeholder="Primer Apellido" name="pApellido">
    </div>
    <div class="input-group mb-3">

      <input type="text" class="form-control" placeholder="Segundo Apellido" name="sApellido">
    </div>

    <legend class="col-form-label col-sm-2 pt-0">Género</legend>
    

      <div class="custom-control custom-radio">
        <input type="radio" id="customRadio1" name="genero" class="custom-control-input" value="Hombre">
        <label class="custom-control-label" for="customRadio1">Hombre</label>
      </div>
      <div class="custom-control custom-radio">
        <input type="radio" id="customRadio2" name="genero" class="custom-control-input" value="Mujer">
        <label class="custom-control-label" for="customRadio2">Mujer</label>
      </div>
      <div class="custom-control custom-radio">
        <input type="radio" id="customRadio3" name="genero" class="custom-control-input" value="Otro">
        <label class="custom-control-label" for="customRadio3">Otro</label>
      </div><br>
    
    <div class="md-form md-outline input-with-post-icon datepicker">
      <label>Fecha de Nacimiento</label>
      <input placeholder="Select date" type="date" name="fecha" class="form-control">

    </div><br>
    <div class="input-group mb-3">

      <input type="password" class="form-control" placeholder="Contraseña" name="password">
    </div>
    <button type="submit" class="btn btn-outline-info" style="width: 10rem;" name="submit">Registrar</button>

  </form>

</body>

</html>